library source_gen.example.example;

import 'package:source_gen/generators/json_serializable.dart';

part 'example.g.dart';

@JsonSerializable()
class Person extends Object with _$PersonSerializerMixin
{
  final String firstName, lastName;

  Person(this.firstName, this.lastName);

  factory Person.fromJson(json) => _$PersonFromJson(json);
}
