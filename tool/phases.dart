import 'package:build_runner/build_runner.dart';

import 'package:source_gen/generators/json_serializable_generator.dart';
import 'package:source_gen/source_gen.dart';

const json = const JsonSerializableGenerator();
final builder = new GeneratorBuilder(const [json]);

const globs = const ['example/*.dart'];
final inputSet = new InputSet('source_gen', globs);

final PhaseGroup phases = new PhaseGroup.singleAction(builder, inputSet);
